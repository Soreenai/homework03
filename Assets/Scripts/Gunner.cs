using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{   
    public class Gunner : MonoBehaviour
    {
        [SerializeField] private float _shotDelay;
        [SerializeField] private GameObject _bullet;

        private float _timer = 0;

        private void Awake()
        {
            _timer = _shotDelay;
            
        }

        private void Update()
        {
            GunnerShoot();
        }

        private void GunnerShoot()
        {
            _timer -= Time.deltaTime;

            if (_timer <= 0)
            {
                _timer = _shotDelay;
                Instantiate(_bullet, transform.position, transform.rotation);
            }
        }

    }
}
