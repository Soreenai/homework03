using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{    public class Teleport : MonoBehaviour
    {
        public GameObject _teleportExit;

        private void OnTriggerEnter(Collider other)
        {
            other.transform.position = _teleportExit.transform.position;
        }
    }

}