﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Game
{
public class Level : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private bool _timerIsOn;
    [SerializeField] private float _timerValue;
    [SerializeField] private Text _timerView;

    [Header("Objects")]
    [SerializeField] private Player _player;
    [SerializeField] private Exit _exitFromLevel;

    [Header("GameOverPanel")]
    [SerializeField] private GameObject _gameOverPanel;
    [SerializeField] private Text _gameWinText;
    [SerializeField] private Text _gameLoseText;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _nextLevelButton;


    private int _sceneID;
    private float _timer = 0;
    private bool _gameIsEnded = false;

    private void Awake()
    {
        _sceneID = SceneManager.GetActiveScene().buildIndex;
        _timer = _timerValue;
    }

    private void Start()
    {
        _exitFromLevel.Close();
    }

    private void Update()
    {
        if(_gameIsEnded)
            return;
        
        TimerTick();
        LookAtPlayerHealth();
        LookAtPlayerInventory();
        TryCompleteLevel();
    }

    private void TimerTick()
    {
        if(_timerIsOn == false)
            return;
        
        _timer -= Time.deltaTime;
        _timerView.text = $"{_timer:F1}";
        
        if(_timer <= 0)
            Lose();
    }

    private void TryCompleteLevel()
    {
        if(_exitFromLevel.IsOpen == false)
            return;

        var flatExitPosition = new Vector2(_exitFromLevel.transform.position.x, _exitFromLevel.transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);
        
        if(flatExitPosition == flatPlayerPosition)
            Victory();
    }

    private void LookAtPlayerHealth()
    {
        if(_player.IsAlive)
            return;

        Lose();
        Destroy(_player.gameObject);
    }

    private void LookAtPlayerInventory()
    {
        if(_player.HasKey)
            _exitFromLevel.Open();
    }

    public void Victory()
    {
        _gameIsEnded = true;
        _player.Disable();
        ShowWinningPanel();
    }

    public void Lose()
    {
        _gameIsEnded = true;
        _player.Disable();
        ShowLosingPanel();
    }

    private void ShowLosingPanel()
    {
        _gameOverPanel.SetActive(true);
        _gameLoseText.gameObject.SetActive(true);
        _restartButton.gameObject.SetActive(true);
    }
    private void ShowWinningPanel()
    {
        _gameOverPanel.SetActive(true);
        _gameWinText.gameObject.SetActive(true);
        _restartButton.gameObject.SetActive(true);
        _nextLevelButton.gameObject.SetActive(true);
    }
    public void RestartLevel()
    {
        
        SceneManager.LoadScene(_sceneID);
    }

    public void NextLevel()
    {
        if (_sceneID < SceneManager.sceneCountInBuildSettings - 1)
        {
            SceneManager.LoadScene(_sceneID + 1);
        }
        else SceneManager.LoadScene(0);
    }
}
}   