using UnityEngine;

namespace Game
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float _bulletSpeed;
        [SerializeField] private float _step;

        private float _timer = 0;

        private void Awake()
        {
            _timer = _bulletSpeed;
        }

        private void Update()
        {
            FlyingBullet();
        }

        private void FlyingBullet()
        {
            _timer -= Time.deltaTime;

            if (_timer <= 0)
            {
                _timer = _bulletSpeed;
                transform.position += transform.forward * _step;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            Destroy(gameObject);
        }
    }
}